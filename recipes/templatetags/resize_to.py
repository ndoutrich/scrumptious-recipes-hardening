from django import template

register = template.Library()


# @register.filter
# def resize_to(value, arg):
#     if arg is not None:
#         return int(value) * int(arg)
#     else:
#         return value


@register.filter
def resize_to(ingredient, arg):
    numberofservings = ingredient.recipe.servings
    amount = ingredient.amount

    if numberofservings is not None and arg is not None:
        ratio = int(arg) / int(numberofservings)
        newamount = ratio * int(amount)
        return newamount
    else:
        return amount
